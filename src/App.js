import React from 'react';
import Header from './components/Header/Header';
import DescriptionRobot from './components/DescriptionRobot/DescriptionRobot';
import Footer from './components/Footer/Footer';
import NoPublicDataPart from './components/NoPublicDataPart/NoPublicDataPart';
import 'bootstrap/dist/css/bootstrap.css';
import './App.scss';


function App() {
  return (
    <div className="App">
      <Header />
      <DescriptionRobot />
      <NoPublicDataPart />
      <Footer />
    </div>
  );
}

export default App;
