import React from 'react';
import './Footer.scss';
import arrowTop from '../../assets/images/top-arrow.png';

function Footer() {
  return (
    <div className="footer-page">


      <div className="container ">
        <div className="row">
          <div className="col-md-10 mx-auto">
            <div className="row">
              <div className="col-md-3">
                <p className="copyright-mark">© 2020 Jungle</p>
              </div>
              <div className="col-md-7">
                <div className="row">
                  <div className="col-md-4">
                    <ul>
                      <li>
                        <a href="/about">About</a>
                      </li>
                      <li>
                        <a href="/blog">Blog</a>
                      </li>
                      <li>
                        <a href="/contacts">Contacts</a>
                      </li>
                      <li>
                        <a href="/privacy-policy">Privacy Policy</a>
                      </li>
                      <li>
                        <a href="/imprint">Imprint</a>
                      </li>
                    </ul>
                  </div>

                  <div className="col-md-4">
                    <ul>
                      <li>
                        <a href="/robots">Robots</a>
                      </li>
                      <li>
                        <a href="/projects">Projects</a>
                      </li>
                      <li>
                        <a href="/join-jungle">Join Jungle</a>
                      </li>
                      <li>
                        <a href="/events">Events</a>
                      </li>
                      <li>
                        <a href="/careers">Careers</a>
                      </li>
                    </ul>
                  </div>

                  <div className="col-md-4">
                    <ul>
                      <li>
                        <a href="/linkedin">Linkedin</a>
                      </li>
                      <li>
                        <a href="/facebook">Facebook</a>
                      </li>
                      <li>
                        <a href="/twitter">Twitter</a>
                      </li>
                      <li>
                        <a href="/instagram">Instagram</a>
                      </li>
                    </ul>
                  </div>
                </div>

              </div>
              <div className="col-md-2">
                <div className="top-button">
                  <img src={ arrowTop } alt="top-icon-arrow"/>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  );
}


export default Footer;
