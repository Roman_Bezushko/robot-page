import React from 'react';
import descriptionImage from '../../assets/images/robot-description.png';
import companyLogo from '../../assets/images/company-logo.png';
import backArrow from '../../assets/images/back-arrow.png';
import './DescriptionRobot.scss';

function DescriptionRobot()  {
  return (
    <div className="container description-page">
      <div className="row">
        <div className="col-md-10 mx-auto">
          <div className="row">
            <div className="col-md-4">
              <a href="/" className="top-links">
                <img src={ backArrow} alt="back-arrow"/>
                <span className=" back-link">back </span>
              </a>

              <img className="description-image" src={ descriptionImage } alt="robot-description-photo"/>
              <img className="company-logo" src={ companyLogo } alt="company-logo"/>
            </div>

            <div className="col-md-7 offset-md-1">
              <a href="/" className="top-links more-link">visor.hr  →</a>
              <h2 className="description-title">Panel Inline 3D Inspection</h2>
              <p className="description-text">
                3D imaging and inspection solution based on laser profiling,
                for various panel manufacturing processes & industries.
                Flexible configurations and customised setup, automated
                analysis and verification against CAD models, robot cell integration.
              </p>

              <div className="list-kinds purple">
                <span>Automotive</span>
                <span>Aerospace</span>
                <span>Plastics</span>
              </div>

              <div className="list-kinds">
                <div className="blue">
                  <span>Visual inspection</span>
                  <span>Quality control</span>
                  <span>Assembly</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

DescriptionRobot.propTypes = {};

export default DescriptionRobot;
