import React from 'react';
import NonPublicDataImage from '../../assets/images/non-public-data.png';
import './NoPublicDataPart.scss';

function NoPublicDataPart() {
  return (
    <div className="grey-background-wrapper">
      <div className="container">
        <div className="row">
          <div className="col-md-10 mx-auto main-wrapper">
            <div className="blue-wrapper">
              <h3>Non-public data</h3>
            </div>
            <div className="row">
              <img className="non-public-image" src={NonPublicDataImage} alt="non-public-data"/>
            </div>

          </div>
        </div>
      </div>
    </div>

  );
}

NoPublicDataPart.propTypes = {};

export default NoPublicDataPart;
