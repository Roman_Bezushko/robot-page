import React, {Component} from 'react';
import logoImage from "../../assets/images/logo-jungle.png"
import {
  Nav,
  Navbar,
} from 'react-bootstrap';
import './Header.scss';

class Header extends Component {
  render() {
    return (
      <header className="header">
        <div className="container">
          <div className="row">
            <div className="col-md-12">

              <Navbar expand="lg">
                <Navbar.Brand href="/">
                  <img className="header-logo" src={logoImage} alt="Logo" />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />

                <Navbar.Collapse id="basic-navbar-nav">
                  <Nav className="ml-auto">
                    <Nav.Link href="/about">About</Nav.Link>
                    <Nav.Link href="/robots">Robots</Nav.Link>
                    <Nav.Link href="/projects">Projects</Nav.Link>
                    <Nav.Link href="/blog">Blog</Nav.Link>
                    <Nav.Link href="/careers">Careers</Nav.Link>
                    <Nav.Link href="/contacts">Contacts</Nav.Link>
                  </Nav>
                </Navbar.Collapse>
              </Navbar>

            </div>
          </div>
        </div>
      </header>
    );
  }
}

Header.propTypes = {};

export default Header;
